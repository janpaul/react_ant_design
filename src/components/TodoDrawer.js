import {
  Breadcrumb,
  Table,
  Button,
  Space,
  Modal,
  Input,
  Select,
  Drawer,
  message,
} from "antd";
import React, { useState, useEffect } from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";

const TodoDrawer = ({ visible, todos_list, onCloseDrawer,user_detatails }) => {
  
  
    const test = [
    { title: "buy goods", date: "2020-09-28 09:45:58" },
    { title: "event 2", date: "2020-09-29" },
  ];

  const data = todos_list.map( (t,idx) =>({
      title:t.todo,
      date:t.created_at
  }));

//   console.log(data)
  return (
    <Drawer
      title={`Todo List's of ${user_detatails.name} ${data.length <= 0 ? `(NO TODO LIST)` : `` }`   }
      width={720}
      onClose={() => onCloseDrawer(false)}
      visible={visible}
      bodyStyle={{ paddingBottom: 80 }}
      footer={
        <div
          style={{
            textAlign: "right",
          }}
        >
          <Button
            onClick={() => onCloseDrawer(false)}
            style={{ marginRight: 8 }}
          >
            Cancel
          </Button>
        </div>
      }
    >
      <FullCalendar
        events={data}
        headerToolbar={{
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
          }}
        plugins={[dayGridPlugin, timeGridPlugin]}
        initialView="timeGridWeek"
      />
    </Drawer>
  );
};

export default TodoDrawer;
